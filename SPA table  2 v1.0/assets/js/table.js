export class TableMovie {
    constructor(jsonMovie, container) {
        this.jsonMovie = jsonMovie;
        const containerMovie = document.querySelector(`#${container}`);
        this.container = containerMovie;
    };
    createMovieTable(jsonMovie) {
        const table = document.createElement("table");
        const thead = document.createElement("thead");
        const tbody = document.createElement("tbody");
        const trhead = document.createElement("tr");
        // take keys for thead
        Object.keys(jsonMovie[0]).forEach(key => {
            const th = document.createElement("th");
            th.textContent = key;
            trhead.append(th);
            th.setAttribute("class", "header-border");
        });
        // take value for body
        jsonMovie.forEach(el => {
            const tr = document.createElement("tr");
            Object.keys(el).forEach(value => {
                let td = document.createElement("td");
                td.textContent = el[value];
                tr.append(td);
                td.setAttribute("class", "data-style")
                if (el[value] == null) {
                    td.textContent = "null"
                };
                tr.addEventListener("click", createForm);
                function createForm() {
                    //modalWindow
                    let modalWindow = document.querySelector(".modal");
                    let modalCloseX = document.querySelector(".close");
                    let modalCloseBtn = document.querySelector(".btn-btn-secondary");
                    let madalBody = document.querySelector(".modal-body");
                    const saveBtn = document.querySelector(".btn-btn-primary");
                    //show modalWinow
                    modalWindow.style.display = "block"
                    tr.removeEventListener("click", () => { });
                    modalCloseX.addEventListener("click", closeBtn);
                    modalCloseBtn.addEventListener("click", closeBtn);
                    saveBtn.addEventListener("click", saveContent);
                    // create form
                    const form = document.createElement("form");
                    let input = document.createElement("input");
                    let div = document.createElement("div");
                    let label = document.createElement("label");
                    label.textContent = value;
                    input.type = "text";
                    input.value = td.textContent;
                    if (el[value] == null) {
                        input.value = td.textContent
                    };
                    div.append(label);
                    div.append(input);
                    form.append(div);
                    madalBody.append(form);
                    // closeBtn
                    function closeBtn() {
                        modalWindow.style.display = "none";
                        form.remove();
                        modalCloseBtn.removeEventListener("click", () => { });
                        modalCloseX.removeEventListener("click", () => { });
                    };
                    //saveBtn
                    function saveContent() {
                        td.textContent = input.value;
                        modalWindow.style.display = "none";
                        form.remove();
                        saveBtn.removeEventListener("click", () => { });
                    };
                };
            });
            tbody.append(tr);
        });
        thead.append(trhead);
        table.append(thead, tbody);
        this.container.append(table);
        //table style
        table.setAttribute("class", "table-border");
    };
};