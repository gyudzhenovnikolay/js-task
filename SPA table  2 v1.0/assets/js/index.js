import { TableMovie } from "./table.js"

fetch("https://raw.githubusercontent.com/vega/vega/master/docs/data/movies.json")
    .then(response => response.json())
    .then(json => {
        let MyTable = new TableMovie(json, "table-container");
        MyTable.createMovieTable(json);
    });    